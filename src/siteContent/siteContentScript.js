var jobSeekers = [

    {
        "Role": "Software Developers & Programmers",
        "numJobs": 570,
        "salaryMax": 100,
        "salaryMin": 72,
        "skills": ["computer software and systems", "programming languages and techniques", "software development processes such as Agile", "confidentiality, data security and data protection issues."],
        "jobDescription": "Software developers and programmers develop and maintain computer software, websites and software applications (apps)."
    },

    {
        "Role": "Database & Systems Administrators",
        "numJobs": 74,
        "salaryMax": 90,
        "salaryMin": 66,
        "skills": ["a range of database technologies and operating systems", "new developments in databases and security systems",
            "computer and database principles and protocols"],
        "jobDescription": "Database & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures."
    },

    {
        "Role": "Help Desk & IT Support",
        "numJobs": 144,
        "salaryMax": 65,
        "salaryMin": 46,
        "skills": ["computer hardware, software, networks and websites", "the latest developments in information technology."],
        "jobDescription": "Information technology (IT) helpdesk/support technicians set up computer and other IT equipment and help prevent, identify and fix problems with IT hardware and software."
    },

    {
        "Role": "Data Analyst",
        "numJobs": 270,
        "salaryMax": 128,
        "salaryMin": 69,
        "skills": ["data analysis tools such as Excel, SQL, SAP and Oracle, SAS or R", "data analysis, mapping and modelling techniques",
            "analytical techniques such as data mining"],
        "jobDescription": "Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims."
    },

    {
        "Role": "Test Analyst",
        "numJobs": 127,
        "salaryMax": 98,
        "salaryMin": 70,
        "skills": ["programming methods and technology", "computer software and systems", "project management"],
        "jobDescription": "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems."
    },

    {
        "Role": "Project Management",
        "numJobs": 188,
        "salaryMax": 190,
        "salaryMin": 110,
        "skills": ["principles of project management", "approaches and techniques such as Kanban and continuous testing", "how to handle software development issues", "common web technologies used by the scrum team."],
        "jobDescription": "Project managers use various methods to keep project teams on track. They also help remove obstacles to progress."
    }
];

function makeMyTable() {

    var tableDiv = document.getElementById("tableContainer");
    table = document.createElement("table");
    table.style.borderStyle = "solid";
    table.id = "theTable";

    var thead = document.createElement("thead");
    var cellhead = document.createElement("th");
    cellhead.colSpan = 4;
    cellhead.innerHTML = "Jobs in ICT click to view more details ";
    var Headrow = document.createElement("tr");
    Headrow.appendChild(cellhead);
    thead.appendChild(Headrow);

    var tbody = document.createElement("tbody");
    tbody.id = "theTableBody";
    var cell1 = document.createElement("td");
    cell1.innerHTML = "Role";
    var cell2 = document.createElement("td");
    cell2.innerHTML = "Jobs Vacancies 08/18";
    var cell3 = document.createElement("td");
    cell3.innerHTML = "Salary Max";
    var cell4 = document.createElement("td");
    cell4.innerHTML = "Salary Min";

    var row = document.createElement("tr");



    row.appendChild(cell1);
    row.appendChild(cell2);
    row.appendChild(cell3);
    row.appendChild(cell4);
    table.appendChild(row);

    var cellSummary1 = document.createElement("td");
    cellSummary1.innerHTML = "";
    var cellSummary2 = document.createElement("td");
    cellSummary2.innerHTML = "Total: " + totalNumJobs();
    var cellSummary3 = document.createElement("td");
    cellSummary3.innerHTML = "Average: " + averageMaxSalary();
    var cellSummary4 = document.createElement("td");
    cellSummary4.innerHTML = "Average: " + averageMinSalary();

    var summaryRow = document.createElement("tr");
    summaryRow.appendChild(cellSummary1);
    summaryRow.appendChild(cellSummary2);
    summaryRow.appendChild(cellSummary3);
    summaryRow.appendChild(cellSummary4);


    table.appendChild(thead);
    table.appendChild(tbody);
    tableDiv.appendChild(table);

    appendRows();
    table.appendChild(summaryRow);
    addRowHandlers();
}

function appendRows() {

    for (var i = 0; i < jobSeekers.length; i++) {

        var row_l = document.createElement("tr");

        var cell1_l = document.createElement("td");
        cell1_l.innerHTML = jobSeekers[i].Role;
        var cell2_l = document.createElement("td");
        cell2_l.innerHTML = jobSeekers[i].numJobs;
        var cell3_l = document.createElement("td");
        cell3_l.innerHTML = jobSeekers[i].salaryMax;
        var cell4_l = document.createElement("td");
        cell4_l.innerHTML = jobSeekers[i].salaryMin;

        row_l.appendChild(cell1_l);
        row_l.appendChild(cell2_l);
        row_l.appendChild(cell3_l);
        row_l.appendChild(cell4_l);
        table.appendChild(row_l);
    }
}

function totalNumJobs() {
    var total = 0;
    for (var i = 0; i < jobSeekers.length; i++) {
        total = jobSeekers[i].numJobs;
    }
    return total;
}

function averageMaxSalary() {
    var minAverage = 0;
    for (var i = 0; i < jobSeekers.length; i++) {
        minAverage = jobSeekers[i].salaryMin;
    }
    return minAverage;
}

function averageMinSalary() {
    var maxAverage = 0;
    for (var i = 0; i < jobSeekers.length; i++) {
        maxAverage = jobSeekers[i].salaryMin;
    }
    return maxAverage;
}

var imageCollection = [
    {
        name: "pic1",
        role: "Software Developer & Programmer",
        Knowledge: "Skills and Knowledge",
        skill1:"-computer software and systems",
        skill2:"-programming languages and techniques",
        skill3:"-software development processes such as Agile",
        skill4:"-confidentiality, data security and data protection issues.",
        JobDescription: "Software developers and programmers develop and maintain computer software, websites and software applications (apps)."
    },

    {
        name: "pic2",
        role: "Database & Systems Administrators",
        Knowledge: "Skills and Knowledge",
        skill1:"-range of database technologies",
        skill2:"-new developments in databases",
        skill3:"-computer and database principles and protocols",
        skill4:"-wide range of operating and security systems",
        JobDescription: "Database & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures."
    },
    {
        name: "pic3",
        role: "Help Desk & IT Support",
        Knowledge: "Skills and Knowledge",
        skill1: "-computer hardware",
        skill2: "-software, networks and websites",
        skill3: "-latest developments in information technology",
        skill4:"",
        JobDescription: "Information technology (IT) helpdesk/support technicians set up computer and other IT equipment and help prevent, identify and fix problems with IT hardware and software."
    },
    {
        name: "pic4",
        role: "Data Analyst",
        Knowledge: "Skills and Knowledge",
        skill1:"-data analysis tools such as Excel",
        skill2:", SQL, SAP and Oracle, SAS or R",
        skill3:"-analytical techniques such as data mining",
        skill4:"-data analysis, mapping and modelling techniques",
        JobDescription: "Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims."
    },
    {
        name: "pic5",
        role: "Test Analyst",
        Knowledge: "Skills and Knowledge",
        skill1:"-programming methods and technology",
        skill2:"-computer software and systems",
        skill3:"-project management",
        skill4:"",
        JobDescription: "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems."
    },
    {
        name: "pic6",
        role: "Project Management",
        Knowledge: "Skills and Knowledge",
        skill1:"-principles of project management",
        skill2:"-techniques such as Kanban and continuous testing",
        skill3:"-how to handle software development issues",
        skill4:"-common web technologies used by the scrum team.",
        JobDescription: "Project managers use various methods to keep project teams on track. They also help remove obstacles to progress."
    }
];

function changeImage(numImage) {


    var featuredImage = document.getElementById("featuredImage");
    featuredImage.src = "./images/" + imageCollection[numImage].name + ".jpg";

    featuredImage.alt = imageCollection[numImage].role;
    document.getElementById("JobTitle").innerHTML = featuredImage.alt;

    featuredImage.alt = imageCollection[numImage].JobDescription;
    document.getElementById("JobDescription").innerHTML = featuredImage.alt;

    featuredImage.alt = imageCollection[numImage].Knowledge;
    document.getElementById("SkillsTitle").innerHTML = featuredImage.alt;

    featuredImage.alt = imageCollection[numImage].skill1;
    document.getElementById("descriptionText1").innerHTML = featuredImage.alt;

    featuredImage.alt = imageCollection[numImage].skill2;
    document.getElementById("descriptionText2").innerHTML = featuredImage.alt;

    featuredImage.alt = imageCollection[numImage].skill3;
    document.getElementById("descriptionText3").innerHTML = featuredImage.alt;

    featuredImage.alt = imageCollection[numImage].skill4;
    document.getElementById("descriptionText4").innerHTML = featuredImage.alt;

    featuredImage.title = imageCollection[numImage].role;

}


function addRowHandlers() {
    var table = document.getElementById("theTable");
    var rows = table.getElementsByTagName("tr");
    for (var i = 2; i < rows.length; i++) {
        var row2 = table.rows[2];
        var row3 = table.rows[3];
        var row4 = table.rows[4];
        var row5 = table.rows[5];
        var row6 = table.rows[6];
        var row7 = table.rows[7];
        row2.onclick = function (myrow) {
            return function () {
                let cell = myrow.getElementsByTagName("td")[0];
                cell.src = "./images/pic1.jpg";
                changeImage(0);
            };
        }(row2);
        row3.onclick = function (myrow) {
            return function () {
                let cell = myrow.getElementsByTagName("td")[0];
                cell.src = "./images/pic2.jpg";
                changeImage(1);
            };
        }(row3);
        row4.onclick = function (myrow) {
            return function () {
                let cell = myrow.getElementsByTagName("td")[0];
                cell.src = "./images/pic3.jpg";
                changeImage(2);
            };
        }(row4);
        row5.onclick = function (myrow) {
            return function () {
                let cell = myrow.getElementsByTagName("td")[0];
                cell.src = "./images/pic4.jpg";
                changeImage(3);
            };
        }(row5);
        row6.onclick = function (myrow) {
            return function () {
                let cell = myrow.getElementsByTagName("td")[0];
                cell.src = "./images/pic5.jpg";
                changeImage(4);
            };
        }(row6);
        row7.onclick = function (myrow) {
            return function () {
                let cell = myrow.getElementsByTagName("td")[0];
                cell.src = "./images/pic6.jpg";
                changeImage(5);
            };
        }(row7);
    }
}
function loadRandomImage() {

    var numImage = Math.floor(Math.random() * (imageCollection.length));
    changeImage(numImage);
}

